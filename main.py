import argparse
import cProfile as profile
import csv
import cStringIO as StringIO
import glob
import logging
import re

import pandas as pd

NAME_INDEX = 0
VALUE_INDEX = 2


def main():
    for fname in glob.iglob('specs/*'):
        specReader = csv.DictReader(open(fname, 'r'), fieldnames=['column name', 'width', 'datatype'])
        column_validators = {}
        column_specs = []
        for column in list(specReader)[1:]:
            if column['datatype'] == 'TEXT':
                colre = '\A.+\Z'
            elif column['datatype'] == 'INTEGER':
                colre = '\A[0-9]+\Z'
            elif column['datatype'] == 'BOOLEAN':
                colre = '\A[01]\Z'
            column_validators[column['column name']] = colre
            column_spec = int(column['width'])
            column_specs.append(column_spec)

        column_names = column_validators.keys()

        datafilename_basename = fname[fname.rindex('/')+1:fname.rindex('.csv')]
        datafilename_glob = 'data/{0}*.txt'.format(datafilename_basename)

        datafilenames = glob.glob(datafilename_glob)

        logging.debug(datafilenames)
        datafilename = datafilenames[0]
        data = pd.read_fwf(datafilename, header=None, widths=column_specs)
        columns = list(data.columns.values)
        results = StringIO.StringIO()
        writer = csv.writer(results)
        writer.writerow(['name', 'valid', 'value'])

        names = data.ix[:, 0].values
        logging.debug('{0} should be the names in our result data'.format(names))

        for index in xrange(len(data)):
            row = data.ix[index].values
            name = names[index]
            regexp = column_validators.values()[index]
            values = row.tolist()
            value = values[VALUE_INDEX]
            try:
                valid = re.compile(regexp).search(str(value))
                logging.debug('Regular expression "{0}" picked to search {1}'.format(str(regexp), value))
                if not valid:
                    writer.writerow([name, False, value])
                else:
                    raise BaseException
            except BaseException, e:
                writer.writerow([name, True, value])
                continue

        results.seek(0)
        final_frame = pd.read_csv(results, header=0)
        return final_frame.to_string(index=False, justify='right')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Importer for clover health data to sqlite3')

    logging.basicConfig(level=logging.DEBUG)
    main()
